import * as express from "express";
import { Request, Response } from "express";
import { hashPassword } from "../hash";
import { SignupService } from "../services/SignupService";

export class SignupRouter {
  constructor(private signupService: SignupService) {}

  public router = () => {
    const router = express.Router();
    router.post("/", this.createUser);
    return router;
  };

  public createUser = async (req: Request, res: Response) => {
    try {
      const hashed_password = await hashPassword(req.body.password);
      await this.signupService.createUser(req.body.username, hashed_password);
      res.json({ isSuccess: true});
    } catch(e){
      res.json({ isSuccess: false, msg:e });
    }
  };
}