import * as express from "express";
import { Request, Response } from "express";
import * as path from "path";
import { WorkoutService } from "../services/WorkoutService";

export class WorkoutRouter {
  constructor(private workoutService: WorkoutService) { }

  public router = () => {
    const router = express.Router();
    router.get("/", this.startWorkout);
    router.get("/camera", this.camera);
    router.get("/finish", this.workOutFinish);
    router.get("/plan", this.getWorkoutPlan)
    router.get("/today", this.getWorkoutToday)
    router.get("/next", this.getNextWorkout)
    router.get("/nextday", this.nextWorkoutDay)
    router.get("/previousWeight", this.getPreviousWeight)
    router.post("/save", this.logWorkouts)
    return router;
  };

  public startWorkout = async (req: Request, res: Response) => {
    return res.sendFile(path.join(__dirname, "../protected/workout/workOutStart.html"));
  };
  public camera = async (req: Request, res: Response) => {
   return res.sendFile(path.join(__dirname, "../protected/workout/main.html"));
  };
  public workOutFinish = async (req: Request, res: Response) => {
    return res.sendFile(path.join(__dirname, "../protected/workout/workOutFinish.html"));
  };

  public getWorkoutPlan = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      const planID = parseInt(req.user["plan_id"]);
      const WorkoutPlan = await this.workoutService.getWorkoutPlan(planID);
      return res.json(WorkoutPlan);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  };

  public getWorkoutToday = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      //const userID = parseInt(req.user);
      const WorkoutToday = await this.workoutService.getTodaysWorkout(1);
      console.log(WorkoutToday);
      return res.json(WorkoutToday);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  }
  public getNextWorkout = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      //const userID = parseInt(req.user);
      const nextWorkout = await this.workoutService.getNextWorkout(1);
      console.log(nextWorkout);
      return res.json(nextWorkout);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  }
  public nextWorkoutDay = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      //const userID = parseInt(req.user);
      const nextWorkout = await this.workoutService.getNextWorkoutDay(1);
      console.log(nextWorkout);
      return res.json(nextWorkout);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  }

  public logWorkouts = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      const userID = parseInt(req.user);
      const logWorkouts = await this.workoutService.logWorkOuts(req.body, userID);
      return res.json(logWorkouts);
    } catch (e) {
      return res.json({ workOutLogged: false, msg: e });
    }
  };

  public getPreviousWeight = async (req: Request, res: Response) => {
    try {
      //console.log(req.user);
      //const userID = parseInt(req.user);
      const logWorkouts = await this.workoutService.getPreviousWorkoutWeight(req.user);
      return res.json(logWorkouts);
    } catch (e) {
      return res.json({ workOutLogged: false, msg: e });
    }
  };
}