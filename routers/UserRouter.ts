import * as express from "express";
import * as path from "path";
import { Request, Response } from "express";
import { UserService } from "../services/UserService";

export class UserRouter {
  constructor(private userService: UserService) {}

  public router = () => {
    const router = express.Router();
    router.get("/user", this.userProfile);
    router.get("/user/profile", this.profilePage);

    router.get("/user/getProfile", this.profileInfo);
    router.get("/user/progress", this.progressPage);
    //router.get("/user/calender", this.CalenderPage);
    router.get("/user/log", this.logPage);
    router.get("/user/setting", this.settingPage);
    router.get("/user/getProgress", this.getProgress);
    //router.get("/user/calender", this.userProfile);
    router.get("/user/getLog", this.getUserLog);
    router.get("/user/getSetting", this.getDeloadandSpotter);
    router.post("/user/postSetting", this.setDeloadandSpotter);
    router.get("/preview", this.showPreview);
    router.post("/preference/choosePlan", this.commitPlan);
    router.get("/preferences", this.getPreferences);
    router.post("/preferences", this.createPreferences);
    router.get("/preference/workoutPlan", this.showMatchedPlan);
    //router.post("/preferences/chooseplan", this.chooseWorkoutPlan);
    //add showmatchedplan route
    //router.get("/user/preference/workoutPlan", this.getMatchPlan)
    return router;
  };

  public userProfile = async (req: Request, res: Response) => {
    try {
      res.sendFile(
        path.join(__dirname, "../protected/userProfile/userProfile.html")
      );
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public profilePage = async (req: Request, res: Response) => {
    try {
      res.sendFile(
        path.join(__dirname, "../protected/userProfile/profile.html")
      );
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public profileInfo = async (req: Request, res: Response) => {
    try {
      let result = await this.userService.getProfileData(req.user);
      res.json(result);
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };
  public progressPage = async (req: Request, res: Response) => {
    try {
      res.sendFile(
        path.join(__dirname, "../protected/userProfile/progress.html")
      );
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };
  public logPage = async (req: Request, res: Response) => {
    try {
      res.sendFile(path.join(__dirname, "../protected/userProfile/log.html"));
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };
  public settingPage = async (req: Request, res: Response) => {
    try {
      res.sendFile(
        path.join(__dirname, "../protected/userProfile/setting.html")
      );
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };
  public getProgress = async (req: Request, res: Response) => {
    try {
      const data = await this.userService.getWorkoutProgress(req.user);
      console.log(data);
      return res.json(data);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  };
  public getUserLog = async (req: Request, res: Response) => {
    try {
      const data = await this.userService.getFullWorkoutLogs(req.user);
      console.log(data);
      return res.json(data);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  };
  public getDeloadandSpotter = async (req: Request, res: Response) => {
    try {
      const data = await this.userService.getSpotterandDeloadStatus(req.user);
      console.log(data);
      return res.json(data);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  };
  public setDeloadandSpotter = async (req: Request, res: Response) => {
    try {
      const data = await this.userService.changeSpotterandDeload(
        req.body.data,
        req.user
      );
      console.log(data);
      return res.json(data);
    } catch (e) {
      return res.json({ isSuccess: false, msg: e });
    }
  };

  public showPreview = async (req: Request, res: Response) => {
    try {
      res.sendFile(path.join(__dirname, "../protected/preview/preview.html"));
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public getPreferences = async (req: Request, res: Response) => {
    try {
      res.sendFile(
        path.join(__dirname, "../protected/preferences/preferences.html")
      );
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public createPreferences = async (req: Request, res: Response) => {
    try {
      console.log(req.body);
      const preference = req.body;
      console.log(req.user);
      await this.userService.createPreferences(req.user, preference);
      await res.json({ isSuccess: true });
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public commitPlan = async (req: Request, res: Response) => {
    try {
      let result = await this.userService.commitPlan(
        req.user,
        req.body.plan_id
      );
      await res.json(result);
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  public showMatchedPlan = async (req: Request, res: Response) => {
    try {
      let result = await this.userService.showMatchedPlan(req.user);
      await res.json(result);
    } catch (e) {
      res.json({ isSuccess: false, msg: e });
    }
  };

  //  public getProfile = async(req: Request, res: Response) => {
  //    try {
  //      // To be completed
  //    } catch(e) {
  //      res.json({ isSuccess: false, msg:e });
  //    }
  //  }
  //
  //  public getProgress = async(req: Request, res: Response) => {
  //    try {
  //      // To be completed
  //    } catch(e) {
  //      res.json({ isSuccess: false, msg:e });
  //    }
  //  }
  //
  //  public getCalendar = async(req: Request, res: Response) => {
  //    try {
  //      // To be completed
  //    } catch(e) {
  //      res.json({ isSuccess: false, msg:e });
  //    }
  //  }
  //
  //  public getLog = async(req: Request, res: Response) => {
  //    try {
  //      // To be completed
  //    } catch(e) {
  //      res.json({ isSuccess: false, msg:e });
  //    }
  //  }
}
