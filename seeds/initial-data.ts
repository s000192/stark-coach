import * as Knex from "knex";

exports.seed = async function (knex: Knex): Promise<any> {
    // Deletes ALL existing entries in goals
    await knex('goals').del();

    // initialize data for goals table
    await knex.batchInsert('goals',[
        // add emojis
        {goal: 'Build the booty size 🍑😋'},
        {goal: 'Lift heavier! 😣💪'},
        {goal: 'Become more endure 😫🤯'}
    ]);

    // Deletes ALL existing entries in levels
    await knex('levels').del();

    // initialize data for levels table
    await knex.batchInsert('levels',[
        // add emojis
        {level: 'admin'},
        {level: 'coach'},
        {level: 'beginner'},
        {level: 'intermediate'},
        {level: 'advanced'},
        {level: 'professional'}
    ]);

    // Deletes ALL existing entries in plan_templates table
    await knex('plan_templates').del();

    // initialize data for plan_templates table
    await knex.batchInsert('plan_templates',[
        // add emojis
        {name: '💪 Stronglift 5x5 💪', primary_goal_id: '2', secondary_goal_id: '1', level_id: '4'},
        {name: '👊 Modified Stronglift 👊', primary_goal_id: '2', secondary_goal_id: '1', level_id: '4'},
        {name: 'Traditional 🍑 building', primary_goal_id: '1', secondary_goal_id: '2', level_id: '3'},
        {name: '👨‍🎓 Endurance Master 👩‍🎓', primary_goal_id: '3', secondary_goal_id: '1', level_id: '3'},
        {name: '💂 German Volume Training 💂', primary_goal_id: '1', secondary_goal_id: '2', level_id: '4'},
        {name: '😎 Smolov Program 🖖', primary_goal_id: '2', secondary_goal_id: '1', level_id: '5'},
        {name: '👍 Westside Barbell Conjugate Method 👍', primary_goal_id: '2', secondary_goal_id: '1', level_id: '5'},
        {name: '👾 5/3/1 👾', primary_goal_id: '2', secondary_goal_id: '1', level_id: '5'},
    ]);

    // Deletes ALL existing entries in users table
    await knex('users').del();

    // initialize data for users table
    await knex.batchInsert('users',[
        {username: 'admin', hashed_password: 'admin', gender: 'N', bodyweight: '71', days_committed: '3', deload: 'true', need_spotter: 'true', level_id: '1', start_date:'2019-05-28' },
    ]);

    // Deletes ALL existing entries in exercises table
    await knex('exercises').del();

    // initialize data for exercises table
    await knex.batchInsert('exercises',[
        {name: '🍑Squat🍑'},
    ]);

    // Deletes ALL existing entries in detailed_plans
    await knex('detailed_plans').del();

    // initialize data for detailed_plans table
    await knex.batchInsert('detailed_plans',[
        // Stronglift 5x5 (Strength) (id = 1)
        {order_of_ex: '0', sets: '5', reps: '5', rest_time: '120',day: '1', ex_id: '1', plan_id: '1'},
        {order_of_ex: '0', sets: '5', reps: '5', rest_time: '120', day: '3', ex_id: '1', plan_id: '1'},
        {order_of_ex: '0', sets: '5', reps: '5', rest_time: '120', day: '5', ex_id: '1', plan_id: '1'},

        // Traditional building (Hypertrophy) (id = 3)
        {order_of_ex: '0', sets: '4', reps: '8', rest_time: '90', day: '1', ex_id: '1', plan_id: '3'},
        {order_of_ex: '0', sets: '4', reps: '8', rest_time: '90', day: '3', ex_id: '1', plan_id: '3'},
        {order_of_ex: '0', sets: '4', reps: '8', rest_time: '90', day: '5', ex_id: '1', plan_id: '3'},

        // Endurance master (20 reps routine) (id = 4)
        {order_of_ex: '0', sets: '4', reps: '20', rest_time: '90', day: '1', ex_id: '1', plan_id: '4'},
        {order_of_ex: '0', sets: '4', reps: '20', rest_time: '90', day: '3', ex_id: '1', plan_id: '4'},
        {order_of_ex: '0', sets: '4', reps: '20', rest_time: '90', day: '5', ex_id: '1', plan_id: '4'}
    ]);

    // Deletes ALL existing entries in risks
    await knex('risks').del();

    // initialize data for risks table
    await knex.batchInsert('risks',[
        {name: 'Your stance is too narrow!'},
        {name: 'Your stance is too wide!'},
        {name: 'Your knee is pointing inward!'},
        {name: 'Your stance is pointing outward!'}
    ]);
};
