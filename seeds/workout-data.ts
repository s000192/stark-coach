import * as Knex from "knex";

exports.seed = async function (knex: Knex): Promise<any> {
    await knex.batchInsert('users_workout',[
        // Stronglift 5x5 (Strength) (id = 1)
        {order_of_ex: '0',weight:50,  sets: '5', reps: '5', rest_time: '120',day: '1', ex_id: '1',  user_id:1},
        {order_of_ex: '0', weight:60, sets: '5', reps: '5', rest_time: '120', day: '3', ex_id: '1',  user_id:1},
        {order_of_ex: '0', weight:70, sets: '5', reps: '5', rest_time: '120', day: '5', ex_id: '1', user_id:1},

    ]);

    await knex.batchInsert('workout_logs',[
        // Stronglift 5x5 (Strength) (id = 1)
        {planned_date: '2019-05-28',planned_weights:50,  planned_reps:'5', executed_date: '2019-05-29', executed_weights: 60,executed_reps: '5', ex_id:1,  user_id:1},
        {planned_date: '2019-05-28',planned_weights:50,  planned_reps:'5', executed_date: '2019-05-29', executed_weights: 70,executed_reps: '5', ex_id:1,  user_id:1}

    ]);
};
