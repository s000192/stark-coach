const app = new Vue({
  el: '#app',
  data: {
    logbook: []
  },
  methods: {
    async fetchData() {
      const res = await fetch("/users/user/getLog");
      const data = await res.json();
      console.log(data);
      this.logbook = data;
    }
  }
});

app.fetchData()
