const Calendar = Vue.component("Calendar", {
    data: function() {
      return {
        calendar: 0
      };
    },
    created(){
      // this.fetchData();
    },
    destroyed(){
        console.log("bye")
    },
    methods: {
      async fetchData() {
        const res = await fetch("/users/user/calendar");
        const data = await res.json();
        this.calendar = data[0];
      }
    },
    template:
      '<h1>Calendar</h1>'
  });