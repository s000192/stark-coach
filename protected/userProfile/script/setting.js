const app = new Vue({
  el: "#app",
  data: {
    setting: { deload: false, need_spotter: true },
    button: "Save"
  },
  methods: {
    async fetchData() {
      const res = await fetch("/users/user/getSetting");
      const data = await res.json();
      this.setting = data[0];
    },
    checkDeload() {
      this.setting.deload = this.setting.deload ? false : true;
    },
    checkDraw() {
      this.setting.need_spotter = this.setting.need_spotter ? false : true;
    },
    async submit() {
      const res = await fetch("/users/user/postSetting", {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({ data: this.setting })
      });
      this.button = "Saved!";
    }
  }
});

app.fetchData()
