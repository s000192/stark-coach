const app = new Vue({
  el: "#app",
  data: {
    progress: [
      ["14-5-2019", "16-5-2019", "20-5-2019", "24-5-2019"],
      [56, 57, 58, 53]
    ]
  },
  methods: {
    async fetchData() {
      const res = await fetch("/users/user/getProgress");
      const data = await res.json();
      this.progress = data[0];
    },
    chart() {
      const ctx = document.getElementById("myChart").getContext("2d");
      const chart = new Chart(ctx, {
        // The type of chart we want to create
        type: "line",

        // The data for our dataset
        data: {
          labels: this.progress[0],
          datasets: [
            {
              label: "My Fitness Progress",
              backgroundColor: "rgb(255, 99, 132)",
              borderColor: "rgb(255, 99, 132)",
              data: this.progress[1]
            }
          ]
        }
      });
    }
  }
});

app.fetchData()
app.chart();
