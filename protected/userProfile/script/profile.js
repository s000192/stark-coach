const app = new Vue({
  el: "#app",
  data: {
    loading: true,
    profile: {
      username: "admin",
      gender: "N",
      weight: 71,
      days_committed: 3,
      start_date: "2019-05-28"
    }
  },
  methods: {
    async fetchData() {
      const res = await fetch("/users/user/getProfile");
      const data = await res.json();
      this.profile = data[0];
      this.loading = false;
    }
  }
});

app.fetchData()
