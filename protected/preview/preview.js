const app = new Vue({
  el: "#app",
  data: {
    workoutPlans: [{ name: "ABC" }, { name: "EFG" }, { name: "Hellow" }],
    chosenPlan: { name: "ABC", sets: 4, reps: 10, rest_time: 60 },
    choosing: true
  },
  methods: {
    async fetchData() {
      const res = await fetch("/users/preference/workoutPlan");
      const data = await res.json();
      this.workoutPlans = data;
    },
    async choosePlan(plan) {
      const res = await fetch("/users/preference/choosePlan", {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({ plan_id: plan.id })
      });
      const data = await res.json();
      this.chosenPlan = data[0];
      this.chosenPlan.name = plan.name;
      this.choosing = false;
    },
    GoWorkout() {
      window.location = "/workout";
    }
  }
});


app.fetchData()