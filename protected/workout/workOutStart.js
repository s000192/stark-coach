Window.onload = async => {

    getWorkoutDetails();
    
    
    //find user's previous weight

    //previous weight + 2.5/5kg = today's weight
}

//find user's workout today and return exercise, reps, sets
getWorkoutDetails = async () => {
const res = await fetch('/workout/today');
    const result = await res.json();
    console.log(result);
    document.querySelector('#ex-name').innerHTML = result[0]['name'];
    document.querySelector('#ex-reps-sets').innerHTML = `${result[0]['sets']} sets ${result[0]['reps']} reps`;
    document.querySelector('#today').innerHTML = result[0]['weight'];
}

getPrevWeight = async () => {
    const res = await fetch('/workout/previousWeight');
        const result = await res.json();
        console.log(result);
        if (result.workOutLogged === false) {
            document.querySelector('#previous').innerHTML = "N/A";
            document.querySelector('#kg').innerHTML = " ";
        } else {
        document.querySelector('#previous').innerHTML = result;
        }
    }
getWorkoutDetails();
getPrevWeight();
