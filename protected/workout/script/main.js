const global = {};

window.onload = async() => {
    global.workout = new Workout();
    await global.workout.initialize();
    global.camera = new Camera();
    app.drawRadar();
    global.camera.bindPage()
}