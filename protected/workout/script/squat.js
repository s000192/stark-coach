class PostureIndicator {
  constructor(pose) {
    this.nose = pose.keypoints[0].position;
    this.leftEye = pose.keypoints[1].position;
    this.rightEye = pose.keypoints[2].position;
    this.leftEar = pose.keypoints[3].position;
    this.rightEar = pose.keypoints[4].position;
    this.leftShoulder = pose.keypoints[5].position;
    this.rightShoulder = pose.keypoints[6].position;
    this.leftElbow = pose.keypoints[7].position;
    this.rightElbow = pose.keypoints[8].position;
    this.leftWrist = pose.keypoints[9].position;
    this.rightWrist = pose.keypoints[10].position;
    this.leftHip = pose.keypoints[11].position;
    this.rightHip = pose.keypoints[12].position;
    this.leftKnee = pose.keypoints[13].position;
    this.rightKnee = pose.keypoints[14].position;
    this.leftAnkle = pose.keypoints[15].position;
    this.rightAnkle = pose.keypoints[16].position;
  }
  //Barry: throughout the squat movement
  shoulderParallelFunct = (tolerance = 0.1) => {
    if (
      (this.leftShoulder.y - this.rightShoulder.y) /
        (this.leftShoulder.x - this.rightShoulder.x) <
        tolerance &&
      (this.leftShoulder.y - this.rightShoulder.y) /
        (this.leftShoulder.x - this.rightShoulder.x) >
        -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };
//Barry: throughout the squat movement
  HipParallelFunct = (tolerance = 0.1) => {
    //hip parallel
    if (
      (this.leftHip.y - this.rightHip.y) / (this.leftHip.x - this.rightHip.x) <
        tolerance &&
      (this.leftHip.y - this.rightHip.y) / (this.leftHip.x - this.rightHip.x) >
        -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };
//Barry: throughout the squat movement
  kneeParallelFunct = (tolerance = 0.1) => {
    //knee parallel
    if (
      (this.leftKnee.y - this.rightKnee.y) /
        (this.leftKnee.x - this.rightKnee.x) <
        tolerance &&
      (this.leftKnee.y - this.rightKnee.y) /
        (this.leftKnee.x - this.rightKnee.x) >
        -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  // Use Math.abs
  kneeParallel = (tolerance = 0.1) =>(
    Math.abs((this.leftKnee.y - this.rightKnee.y) /
    (this.leftKnee.x - this.rightKnee.x)) < tolerance
  )
       
//Barry: throughout the squat movement
  ankleParallelFunct = (tolerance = 0.1) => {
    //ankle parallel
    if (
      (this.leftAnkle.y - this.rightAnkle.y) /
        (this.leftAnkle.x - this.rightAnkle.x) <
        tolerance &&
      (this.leftAnkle.y - this.rightAnkle.y) /
        (this.leftAnkle.x - this.rightAnkle.x) >
        -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };
//Barry: throughout the squat movement
  ankleDistEqualShoulderDist = (tolerance = 0.3) => {
    const shoulderDistance = this.leftShoulder.x - this.rightShoulder.x;
    const ankleDistance = this.leftAnkle.x - this.rightAnkle.x;
    //ankle distance approx. equal to shoulder distance
    if (
      ankleDistance / shoulderDistance > 1 - tolerance &&
      ankleDistance / shoulderDistance < 1 + tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  //determineKneePosition = ()

//Barry: only when down and up (not starting and ending position)
  turningFeetOutWhenDownLeft = (tolerance1 = 0.1, tolerance2 = 0.2) => {
    const result = this.findAnkleKneeSlope(
      this.leftAnkle.x,
      this.leftKnee.x,
      this.leftAnkle.y,
      this.leftKnee.y
    );
    //left ankle slope with left knee & right ankle slop with right knee
    if (result > tolerance1) {
      return false;
    } else if (result < -tolerance2) {
      return true;
    } else {
      return;
    }
  };

//Barry: only when down and up (not starting and ending position)
  turningFeetOutWhenDownRight = (tolerance1 = 0.1, tolerance2 = 0.2) => {
    //left ankle slope with left knee & right ankle slop with right knee
    const result = this.findAnkleKneeSlope(
      this.rightAnkle.x,
      this.rightKnee.x,
      this.rightAnkle.y,
      this.rightKnee.y
    );
    //left ankle slope with left knee & right ankle slop with right knee
    if (result < -tolerance1) {
      return false;
    } else if (result > tolerance2) {
      return true;
    } else {
      return;
    }
  };

  findAnkleKneeSlope = (x2, x1, y2, y1) => {
    return (x2 - x1) / (y2 - y1);
  };
//for count only
  squatCount = (tolerance = 0.9) => {
    //hip get near/below knee level
    if (
      this.leftHip.y / this.leftKnee.y >= tolerance &&
      this.rightHip.y / this.rightKnee.y >= tolerance
    ) {
      // Add Indicators
      return true;
    } else {
      return false;
    }
  };
//for count only
  squatUp = (tolerance = 0.4) => {
    let leftRatio =
      (this.leftHip.y - this.leftKnee.y) / (this.leftHip.y - this.leftAnkle.y);
    let rightRatio =
      (this.rightHip.y - this.rightKnee.y) /
      (this.rightHip.y - this.rightAnkle.y);

    if (leftRatio >= tolerance && rightRatio >= tolerance) {
          // Add Indicators
      return true;
    } else {
      return false;
    }
  };
}
