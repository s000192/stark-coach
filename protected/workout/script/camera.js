navigator.getUserMedia =
  navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia;

class Camera {
  constructor() {
    this.videoWidth = 1280;
    this.videoHeight = 720;
    this.ctx = null;
    this.net = null;
    this.video = null;
  }

  setupCamera = async () => {
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
      throw new Error(
        "Browser API navigator.mediaDevices.getUserMedia not available"
      );
    }

    const video = document.getElementById("video");
    video.width = this.videoWidth;
    video.height = this.videoHeight;

    const stream = await navigator.mediaDevices.getUserMedia({
      audio: false,
      video: {
        facingMode: "user",
        width: this.videoWidth,
        height: this.videoHeight
      }
    });
    video.srcObject = stream;

    return new Promise(resolve => {
      video.onloadedmetadata = () => {
        resolve(video);
      };
    });
  };

  loadVideo = async () => {
    const video = await this.setupCamera();
    video.play();
    return video;
  };

  detectPoseInRealTime = async () => {
    const canvas = document.getElementById("output");
    this.ctx = canvas.getContext("2d");

    canvas.width = this.videoWidth;
    canvas.height = this.videoHeight;

    const poseDetectionFrame = async () => {
      if (app.setting.continue){
        let poses = [];
        let minPoseConfidence = 0.3;
        let minPartConfidence = 0.3;
        const pose = await this.net.estimateSinglePose(this.video, 1, true, 32);
        poses = poses.concat(pose);
        console.log(pose);
  
        // Apply posture test
        global.workout.postureTest(pose);
  
        // Draw Keypoints and Skeleton
        clearCanvas(this.video, this.ctx, this.videoWidth, this.videoHeight);
        poses.forEach(({ score, keypoints }) => {
          if (score >= minPoseConfidence) {
            if (app.setting.drawKeypoints){
              drawKeypoints(keypoints, minPartConfidence, this.ctx);
            }
            if (app.setting.drawSkeleton) {
              drawSkeleton(keypoints, minPartConfidence, this.ctx);
            }
          }
        });
      }
      requestAnimationFrame(poseDetectionFrame);
    };

    poseDetectionFrame();
  };

  bindPage = async () => {
    this.net = await posenet.load();
    this.video = await this.loadVideo();
    this.detectPoseInRealTime();
  };
}
