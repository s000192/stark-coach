const app = new Vue({
  el: "#app",
  data: {
    workoutPlan: {
      // posture: "Squat",
      reps: 8,
      sets: 4,
      rest_time: 50,
      weight: 20
    },
    current: {
      confidence: 0.4,
      posture: "Squat",
      count: 0,
      set: 0,
      state: 0,
      finished: false,
      indicators: {
        shoulderParallelFunct: { correct: 0, wrong: 0 },
        HipParallelFunct: { correct: 0, wrong: 0 },
        kneeParallelFunct: { correct: 0, wrong: 0 },
        ankleParallelFunct: { correct: 0, wrong: 0 },
        ankleDistEqualShoulderDist: { correct: 0, wrong: 0 },
        turningFeetOutWhenDownLeft: { correct: 0, wrong: 0 },
        turningFeetOutWhenDownRight: { correct: 0, wrong: 0 },
        findAnkleKneeSlope: { correct: 0, wrong: 0 }
      }
    },
    signalColors: {
      noSignal: "grey",
      poorSignal: "red",
      normalSignal: "yellow",
      goodSignal: "green"
    },
    setting: {
      drawSkeleton: true,
      drawKeypoints: true,
      continue: true
    },
    pose: { score: 0 }
  },
  computed: {
    progress() {
      return (this.current.count / this.workoutPlan.reps) * 100;
    },
    posenetSignal() {
      if (this.pose.score > 0.7) {
        return this.signalColors.goodSignal;
      } else if (this.pose.score > 0.5) {
        return this.signalColors.normalSignal;
      } else if (this.pose.score > 0.3) {
        return this.signalColors.poorSignal;
      } else {
        return this.signalColors.noSignal;
      }
    },
    shoulderParallelFunctPercent() {
      return (
        this.current.indicators.shoulderParallelFunct.correct /
        (this.current.indicators.shoulderParallelFunct.correct +
          this.current.indicators.shoulderParallelFunct.wrong)
      );
    },
    HipParallelFunctPercent() {
      return (
        this.current.indicators.HipParallelFunct.correct /
        (this.current.indicators.HipParallelFunct.correct +
          this.current.indicators.HipParallelFunct.wrong)
      );
    },
    kneeParallelFunctPercent() {
      return (
        this.current.indicators.kneeParallelFunct.correct /
        (this.current.indicators.kneeParallelFunct.correct +
          this.current.indicators.kneeParallelFunct.wrong)
      );
    },
    ankleParallelFunctPercent() {
      return (
        this.current.indicators.ankleParallelFunct.correct /
        (this.current.indicators.ankleParallelFunct.correct +
          this.current.indicators.ankleParallelFunct.wrong)
      );
    },
    ankleDistEqualShoulderDistPercent() {
      return (
        this.current.indicators.ankleDistEqualShoulderDist.correct /
        (this.current.indicators.ankleDistEqualShoulderDist.correct +
          this.current.indicators.ankleDistEqualShoulderDist.wrong)
      );
    }
  },
  methods: {
    initialize() {
      this.current.count = 0;
      this.current.set = 0;
      this.current.indicators.shoulderParallelFunct.correct = 0;
      this.current.indicators.shoulderParallelFunct.wrong = 0;
      this.current.indicators.HipParallelFunct.correct = 0;
      this.current.indicators.kneeParallelFunct.correct = 0;
      this.current.indicators.ankleParallelFunct.correct = 0;
      this.current.indicators.ankleDistEqualShoulderDist.correct = 0;
      this.current.indicators.turningFeetOutWhenDownLeft.correct = 0;
      this.current.indicators.turningFeetOutWhenDownRight.correct = 0;
      this.current.indicators.findAnkleKneeSlope.correct = 0;
      this.current.indicators.HipParallelFunct.wrong = 0;
      this.current.indicators.kneeParallelFunct.wrong = 0;
      this.current.indicators.ankleParallelFunct.wrong = 0;
      this.current.indicators.ankleDistEqualShoulderDist.wrong = 0;
      this.current.indicators.turningFeetOutWhenDownLeft.wrong = 0;
      this.current.indicators.turningFeetOutWhenDownRight.wrong = 0;
      this.current.indicators.findAnkleKneeSlope.wrong = 0;
      this.drawRadar();
    },
    stop() {
      this.setting.continue = this.setting.continue ? false : true;
    },
    countUp() {
      this.current.count++;
    },
    setUp() {
      if (
        this.current.count === this.workoutPlan.reps &&
        this.current.set < this.workoutPlan.sets
      ) {
        this.current.count = 0;
        this.current.set++;
      } else {
        return;
      }
    },
    async save() {
      const data = {};
      data.weight = this.workoutPlan.weight;
      data.set = this.current.set;
      data.reps = this.current.count;
      data.ex_id = this.workoutPlan.ex_id;
      data.user_id = this.workoutPlan.user_id;

      const res = fetch("/workout/save", {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({ data: data })
      });
    },
    toggleDrawSkeleton() {
      this.setting.drawSkeleton = this.setting.drawSkeleton ? false : true;
    },
    toggleDrawKeypoints() {
      this.setting.drawKeypoints = this.setting.drawKeypoints ? false : true;
    },
    drawRadar() {
      const ctx = document.getElementById("myChart").getContext("2d");
      const chart = new Chart(ctx, {
        type: "radar",
        data: {
          labels: [
            "shoulderParallel",
            "HipParallel",
            "kneeParallel",
            "ankleShoulderDisteEqual",
            "ankleParallel"
          ],
          datasets: [
            {
              label: "Posture Indicators",
              backgroundColor: "rgba(255, 99, 132, 0.3)",
              borderColor: "rgba(255, 99, 132, 0.3)",
              data: [
                this.shoulderParallelFunctPercent,
                this.HipParallelFunctPercent,
                this.kneeParallelFunctPercent,
                this.ankleDistEqualShoulderDistPercent,
                this.ankleParallelFunctPercent
              ]
            }
          ]
        },
        options: {
          scale: {
            // Hides the scale
            display: true
          }
        }
      });
    }
  }
});

// TO DO
// 1. set signal light
// 2. set rest count down
// 3. set settings - mark, skeleton, voice
// 4. set voice
// 5. set user guide - modal
// 6. set give up - modal
