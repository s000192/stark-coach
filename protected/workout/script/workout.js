class Workout {
  constructor() {}

  initialize = async () => {
    // await this.getworkoutPlan();
  };

  getworkoutPlan = async () => {
    const res = await fetch("/workout/plan");
    app.workoutPlan = await res.json();
  };

  postureTest = async pose => {
    const posture = new PostureIndicator(pose);
    app.pose = pose;

    // Pose confidence singal light
    app.setUp();

    // Posture Detection Signals
    // SquatDownSignal
    if (posture.squatCount()) {
      document.querySelector("#signal2").style.backgroundColor = "green";
    } else {
      document.querySelector("#signal2").style.backgroundColor = "red";
    }

    // SquatUpSignal
    if (posture.squatUp()) {
      document.querySelector("#signal3").style.backgroundColor = "green";
    } else {
      document.querySelector("#signal3").style.backgroundColor = "red";
    }
    // Shoulder parallel
    if (posture.shoulderParallelFunct()) {
      document.querySelector("#signal4").style.backgroundColor = "green";
    } else {
      document.querySelector("#signal4").style.backgroundColor = "red";
    }

    // End Game
    if (
      app.current.count === app.workoutPlan.reps &&
      app.current.set === app.workoutPlan.sets
    ) {
      app.save();
      setTimeout(() => {
        $("#endTrainingModal").modal("show");
      }, 1000);
    }

    // Counting the posture
    if (app.pose.score > app.current.confidence) {
      if (
        app.pose.keypoints[11].score > app.current.confidence &&
        app.pose.keypoints[12].score > app.current.confidence &&
        app.pose.keypoints[13].score > app.current.confidence &&
        app.pose.keypoints[14].score > app.current.confidence
      ) {
        // detect squat down
        if (posture.squatCount() && app.current.state === 0) {
          app.current.state = 1;

          if (posture.shoulderParallelFunct()) {
            app.current.indicators.shoulderParallelFunct.correct++;
          } else {
            app.current.indicators.shoulderParallelFunct.wrong++;
          }

          if (posture.HipParallelFunct()) {
            app.current.indicators.HipParallelFunct.correct++;
          } else {
            app.current.indicators.HipParallelFunct.wrong++;
          }

          if (posture.kneeParallelFunct()) {
            app.current.indicators.kneeParallelFunct.correct++;
          } else {
            app.current.indicators.kneeParallelFunct.wrong++;
          }

          if (posture.ankleParallelFunct()) {
            app.current.indicators.ankleParallelFunct.correct++;
          } else {
            app.current.indicators.ankleParallelFunct.wrong++;
          }

          if (posture.ankleDistEqualShoulderDist()) {
            app.current.indicators.ankleDistEqualShoulderDist.correct++;
          } else {
            app.current.indicators.ankleDistEqualShoulderDist.wrong++;
          }
          app.drawRadar();
          document.querySelector("#signal2").style.backgroundColor = "yellow";
        }
        if (
          app.pose.keypoints[15].score > app.current.confidence &&
          app.pose.keypoints[16].score > app.current.confidence
        ) {
          // detect squat up and count
          if (app.current.state === 1 && posture.squatUp()) {
            app.current.state = 0;
            app.countUp();

            if (posture.shoulderParallelFunct()) {
              app.current.indicators.shoulderParallelFunct.correct++;
            } else {
              app.current.indicators.shoulderParallelFunct.wrong++;
            }

            if (posture.HipParallelFunct()) {
              app.current.indicators.HipParallelFunct.correct++;
            } else {
              app.current.indicators.HipParallelFunct.wrong++;
            }

            if (posture.kneeParallelFunct()) {
              app.current.indicators.kneeParallelFunct.correct++;
            } else {
              app.current.indicators.kneeParallelFunct.wrong++;
            }

            if (posture.ankleParallelFunct()) {
              app.current.indicators.ankleParallelFunct.correct++;
            } else {
              app.current.indicators.ankleParallelFunct.wrong++;
            }

            if (posture.ankleDistEqualShoulderDist()) {
              app.current.indicators.ankleDistEqualShoulderDist.correct++;
            } else {
              app.current.indicators.ankleDistEqualShoulderDist.wrong++;
            }
            app.drawRadar();
            document.querySelector("#signal3").style.backgroundColor = "yellow";
          }
        }
      }
    }
  };
}
