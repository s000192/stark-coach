

//check req.user

//find user's plan

//find user's next workout and return exercise, reps, sets

//find user's next weight

getNextWorkout = async () => {
    const res1 = await fetch('/workout/next');
    const result1 = await res1.json();
    console.log(result1);

    const res2 = await fetch('/workout/nextday');
    const result2 = await res2.json();
    console.log(result2);

    document.querySelector('#next-date').innerHTML = result2;
    document.querySelector('#ex-name').innerHTML = result1[0]['name'];
    document.querySelector('#ex-reps-sets').innerHTML = `${result1[0]['sets']} sets ${result1[0]['reps']} reps`;
    document.querySelector('#next').innerHTML = result1[0]['weight'];
}
getNextWorkout();