class CreatePreferencesForm {
    constructor() {
      this.element = document.querySelector("#preferences-form");
      this.element.addEventListener("submit", event => {
        this.onSubmit(event);
        console.log("triggered submit event");
      });
    }
  
    async onSubmit(event) {
      event.preventDefault();
      const form = event.target;
      const data = {
        gender: form["gender"].value,
        weight: form["weight"].value,
        level: form["level"].value,
        goal: form["goal"].value,
        daysCommitted: form["daysCommitted"].value
      };
      const res = await fetch("/users/preferences", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
      const result = await res.json();
  
      if (result.isSuccess) {
        this.element.reset();
        window.location = "/users/preview";
      } else {
        document.querySelector("#error-message").innerHTML =
          "Sorry. There might be some error. Please try again";
      }
    }
  }
  
  window.onload = () => {
    const createPreferencesForm = new CreatePreferencesForm();
  };
  
  function debug() {
    console.log(global.createPreferencesForm);
  }