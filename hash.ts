import * as bcrypt from 'bcrypt';

const SALT_ROUNDS = 10;

// Hash the password to a hash
// Use when account is opened
export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};

// Check if a plaintext has the same hash as the hash stored
// Use when login 
export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}
