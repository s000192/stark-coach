// method One: Use window.location
async function checkLoginStatus(){
    const res = await fetch('/login-status');
    const result = await res.json();

    if(result.isLoggedIn){
       window.location = "/workOutStart.html";
    }else{
        window.location = "/login/login.html";
    }
}

checkLoginStatus();