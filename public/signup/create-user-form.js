class CreateUserForm {
  constructor() {
    this.element = document.querySelector("#sign-up-form");
    this.element.addEventListener("submit", event => this.onSubmit(event));
  }

  async onSubmit(event) {
    event.preventDefault();
    const form = event.target;
    const data = {
      username: form["username"].value,
      password: form["password"].value
    };
    const res = await fetch("/signup", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const result = await res.json();

    if (result.isSuccess) {
      this.element.reset();
      window.location = "/transition";
    } else {
      document.querySelector("#error-message").innerHTML =
        `Username [${form["username"].value}] has been used`;
    }
  }
}
