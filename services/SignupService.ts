// import * as path from "path";
import * as Knex from "knex";
// import { User } from "./models";
// const usersJSON = path.join(__dirname, "users.json");

export class SignupService {
    constructor(private knex: Knex) { }

    public async createUser(newUser: string, hashed_password: string): Promise<object | any> {
        // can use sql to select
        const users: [] = await this.knex.select('username').from('users');
        const existingUser = users.find(
            user => newUser === user["username"]
        );
        if (existingUser) {
            throw new Error(`User ${newUser} already exists`);
        } else {
            console.log('New user is available');
            await this.knex.insert({
                username: newUser,
                hashed_password: hashed_password,
            }).into('users').returning('id');
            console.log('New user is inserted');
        }
    }
}