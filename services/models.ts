export interface User {
    id: number;
    username: string;
    hashed_password: string;
    gender: string;
    bodyweight: number;
    days_committed: number;
    deload: boolean;
    need_spotter: boolean;
    level_id: number;
    goal_id?: number;
    plan_id?: number;
    created_at: string;
    updated_at: string
}

export interface WorkoutPlan {
    id: number,
    order_of_ex: number,
    sets: number,
    reps: number,
    rest_time: number,
    day: number,
    week?: number,
    ex_id: number,
    plan_id: number,
    created_at: string,
    updated_at: string
}

export interface TodaysExercise {
    id: number,
    order_of_ex: number,
    sets: number,
    reps: number,
    rest_time: number,
    day: number,
    week: number,
    ex_id: number,
    user_id: number,
    created_at: string,
    updated_at: string,
    name:string
}

export interface Preference {
    gender: string,
    weight: string,
    level: string,
    goal: string,
    daysCommitted: string
}

export interface PlanTemplates {
    id: number,
    name: string,
    weight_increment: number,
    primary_goal_id: number,
    secondary_goal_id: number,
    level_id: number,
    created_at: string,
    updated_at: string
}

// export interface IData
export interface dataObject {
    weight: number,
    reps:number,
    ex_id:number
}

// export IDeload 
export interface deloadObject {
    deload: boolean,
    need_spotter:boolean
}
