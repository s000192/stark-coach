// import * as path from "path";
import * as Knex from "knex";
import { User, Preference, PlanTemplates, deloadObject } from "./models";
// const usersJSON = path.join(__dirname, "users.json");

export class UserService {
    constructor(private knex: Knex) { }

    public async listUser(): Promise<User[]> {
        // can allow error to be thrown such that they are caught
        try {
            return await this.knex.select('*').from('users');
        } catch (e) {
            return e;
        }

    }

    public async createPreferences(userID: number, preference: Preference) {
        try {
            const levelID = this.knex.select('id').from('levels').where('level', preference.level);
            await this.knex("users")
                .update({
                    gender: preference.gender,
                    bodyweight: parseInt(preference.weight),
                    level_id: levelID,
                    goal_id: preference.goal,
                    days_committed: preference.daysCommitted
                }).where({ id: userID })
        } catch (e) {
            return e;
        }
    }


    public async showUserDetail(userID: number): Promise<User> {
        try {
            const userDetail = await this.knex.first('*').from('users').where('id', userID);
            console.log(userDetail);
            return userDetail;
        } catch (e) {
            console.log(e);
            // throw e or not writing try/catch at all
            return e;
        }
    }

    public async commitPlan(userID: number, planID: number): Promise<object[]> {
        try {
            // Should be inside transaction because there are more than 1 SQL
            //add exercises into usersWorkout as well
            let planDetails = await this.knex.select('*').from('detailed_plans').where({ 'plan_id': planID })
            //console.log(planDetails);
            // Should be a for loop
            let obj1 = {
                order_of_ex: 0,
                weight: 60,
                sets: planDetails[0]['sets'],
                reps: planDetails[0]['reps'],
                rest_time: planDetails[0]['rest_time'],
                day: planDetails[0]['day'],
                ex_id: planDetails[0]['ex_id'],
                user_id: userID
            };
            let obj2 = {
                order_of_ex: 0,
                weight: 70,
                sets: planDetails[1]['sets'],
                reps: planDetails[1]['reps'],
                rest_time: planDetails[1]['rest_time'],
                day: planDetails[1]['day'],
                ex_id: planDetails[1]['ex_id'],
                user_id: userID
            };
            let obj3 = {
                    order_of_ex: 0,
                    weight: 80,
                    sets: planDetails[2]['sets'],
                    reps: planDetails[2]['reps'],
                    rest_time: planDetails[2]['rest_time'],
                    day: planDetails[2]['day'],
                    ex_id: planDetails[2]['ex_id'],
                    user_id: userID
            }
            let objArr = [obj1, obj2, obj3];
            
            await this.knex.batchInsert('users_workout', objArr );
            await this.knex("users").update({ 'plan_id': planID }).where({ 'id': userID });
            console.log(`update plan details success!`);
            console.log(objArr);
            return objArr;
        } catch (e) {
            return e;
        }
    }

    public async showMatchedPlan(userID: number): Promise<PlanTemplates[]> {
        try {
            // Two select can use no transaction
            const getGoalID = await this.knex.select('goal_id','username').from('users').where('id', userID);
            console.log(getGoalID);
            const goalID = getGoalID[0]['goal_id'];
            console.log(goalID);
            const matchedPlans = await this.knex.select('*').from('plan_templates').where('primary_goal_id', goalID);
            console.log(matchedPlans);
            return matchedPlans;
        } catch (e) {
            return e;
        }
    }
    // public async checkGoal(userID: number): Promise<number> {
    //     try {
    //         const goal = await this.knex.first('goal_id').from('users').where('id', userID);
    //         console.log(goal);
    //         return goal;
    //     } catch (e) {
    //         return e;
    //     }
    // }

    // public async checkPlan(userID: number): Promise<number> {
    //     try {
    //         const plan = await this.knex.first('plan_id').from('users').where('id', userID)
    //         console.log(plan)
    //         return plan;
    //     } catch (e) {
    //         return e;
    //     }
    // }

    getProfileData = async (userID: number): Promise<object[]> => {
        try {
            let result = await this.knex.select('*').from('users').where({ 'id': userID });
            console.log('profile data')
            console.log(result);
            return result;
        } catch (e) {
            return e;
        }
    }
    getWorkoutProgress = async (userID: number): Promise<object[]> => {
        try {
            let result = await this.knex.select('executed_date', 'executed_weights', 'exercises.name').from('workout_logs').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID });
            console.log('progress');
            console.log(result);
            return result;
        } catch (e) {
            return e;
        }
    }
    getFullWorkoutLogs = async (userID: number): Promise<object[]> => {
        try {
            let result = await this.knex.select('executed_date', 'executed_weights', 'executed_reps', 'user_id', 'exercises.name').from('workout_logs').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID });
            console.log('fullLog');
            console.log(result);
            return result;
        } catch (e) {
            return e;
        }
    }

    getSpotterandDeloadStatus = async (userID: number): Promise<object[]> => {
        try {
            let result = await this.knex.select('deload', 'need_spotter').from('users').where({ 'id': userID });
            console.log('LoadStatus');
            console.log(result);
            return result;
        } catch (e) {
            return e;
        }
    }

    changeSpotterandDeload = async (object: deloadObject, userID: number): Promise<string> => {
        try {
            console.log(object);
            await this.knex('users').update({
                deload: object.deload,
                need_spotter: object.need_spotter
            }).where({ 'id': userID });
            // normally return id or numbers of rows updated , normally not SQL
            return 'deload/need_spotter status changed'
        } catch (e) {
            return e;
        }
    }




}