import * as Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["testing"]);
import { UserService } from './UserService';

describe("UserService", ()=>{
    let userService:UserService;

    beforeEach(async()=>{
        userService = new UserService(knex);
        // await knex.select('*').from("users").del();
        // await knex.insert({username:"testing123",hashed_password:"$2b$10$.8OXlPVo/rgUIzA8V.jyBeP59.59MTIWUT3hoyPye0TL8f7zQVmOC", gender:"M", bodyweight: "65", days_committed: "2", deload:"false", need_spotter:"true", level_id:"3", goal_id: "3", plan_id:"1"})
        // .into("users");
        while (await knex.migrate.currentVersion() != 'none') {
            await knex.migrate.rollback();
          }
          await knex.migrate.latest();
          await knex.seed.run();        
    })

    it("should get all users", async()=>{
        const users = await userService.listUser();
        expect(users.length).toBe(1);
    });

    afterAll(()=>{
        knex.destroy();
    })
})