// import * as path from "path";
import * as Knex from "knex";
import { WorkoutPlan, TodaysExercise, dataObject } from "./models";
import * as moment from 'moment'


export class WorkoutService {
    constructor(private knex: Knex) { }

    public async getWorkoutPlan(planID: number): Promise<WorkoutPlan[]> {
        const WorkoutPlan = await this.knex.select('*').from('detailed_plans').where('plan_id', planID)
        console.log(WorkoutPlan);
        return WorkoutPlan;
    }

    public async getTodaysWorkout(userID: number): Promise<TodaysExercise[] | string> {

        let now = moment(new Date()).format('YYYY MM DD');
        let nowArr = now.split(' ').map(x => parseInt(x) -1)
        const getStartDate = await this.knex.select('start_date').from('users').where('id', userID);
        const date = moment(getStartDate[0]['start_date']).format('YYYY MM DD');
        let startArr = date.split(' ').map(x => parseInt(x) -1)
        
        //if to test, need to alter the '+1' in order to get output everyday
        let dateDiff = moment(nowArr).diff(startArr, 'days') /* +1 */;
        //const getTodaysWorkout = await this.knex.select('*').from('users_workout').where('user_id', userID);
        //console.log(nowArr);
        //console.log(startArr);
        console.log(dateDiff);
        // should be a for-loop
        if( dateDiff >= 30) { 
            // throw new Error('you have completed your workout cycle')
            return 'you have completed your workout cycle';
        } else if (dateDiff == 1 || dateDiff % 6 == 1) {
            let result1 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 1 });
            console.log(result1);
            return result1;
        } else if (dateDiff == 3 || dateDiff % 6 == 3) {
            let result3 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 3 });
            console.log(result3);
            return result3;
        } else if (dateDiff == 5 || dateDiff % 6 == 5) {
            let result5 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 5 });
            console.log(result5);
            return result5;
        } else {
            return 'you have no workout for today';
        }

    }
    public async getNextWorkout(userID: number): Promise<TodaysExercise[] | string> {

        let now = moment(new Date()).format('YYYY MM DD');
        let nowArr = now.split(' ').map(x => parseInt(x) -1)
        const getStartDate = await this.knex.select('start_date').from('users').where('id', userID);
        const date = moment(getStartDate[0]['start_date']).format('YYYY MM DD');
        let startArr = date.split(' ').map(x => parseInt(x) -1)
        //if to test, need to alter the '+1' in order to get output everyday
        let dateDiff = moment(nowArr).diff(startArr, 'days') /* +1 */;
        //const getTodaysWorkout = await this.knex.select('*').from('users_workout').where('user_id', userID);
        //console.log(now);
        //console.log(dateDiff + 1);
        //algorithm based on 3 day(1/3/5) assumption
        // should be a for-loop
        if( dateDiff >= 30) { 
            return 'you have completed your workout cycle';
        } else if (dateDiff == 1 || dateDiff % 6 == 1) {
            let result1 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 3 });
            console.log(result1);
            return result1;
        } else if (dateDiff == 3 || dateDiff % 6 == 3) {
            let result3 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 5 });
            console.log(result3);
            return result3;
        } else if (dateDiff == 5 || dateDiff % 6 == 5) {
            let result5 = await this.knex.select('*').from('users_workout').join('exercises', 'ex_id', 'exercises.id').where({ 'user_id': userID, 'day': 1 });
            console.log(result5);
            return result5;
        } else {
            //  throw new Error('you have no workout for today')
            return 'you have no workout for today';
        }

    }

    public getNextWorkoutDay = async (userID:number):Promise<string> =>{
        const thisWorkOut = await this.getTodaysWorkout(userID);
        const nextWorkOut = await this.getNextWorkout(userID)
        let Interval = Math.abs(thisWorkOut[0]['day'] - nextWorkOut[0]['day'])
        const nextDate = moment(new Date()).add(Interval,'days').format("DD MMMM YYYY");
        console.log(nextDate);
        return nextDate;
    }

    public getPreviousWorkoutWeight = async (userID:number):Promise<number | string> =>{
       let callResult = await this.knex.select('executed_weights', 'exercises.name','executed_date').from('workout_logs')
       .join('exercises', 'ex_id', 'exercises.id')
       .whereRaw(`user_id = ${userID} and executed_date = (select max(executed_date) from workout_logs)`)
        let result = callResult[0]['executed_weights'];
        if (typeof(result) !== "number") {
            console.log(result);
            return `N/A`;
        } else {
            console.log(result);
            return result;
        }

    }

    public logWorkOuts = async (dataObject: dataObject, userID:number): Promise<string> => {
        let now = moment(new Date()).format('YYYY MM DD');
        let plannedData = await this.getTodaysWorkout(userID);
        console.log(plannedData);
        //planned date currently hard-code(dunno where store this data)
        await this.knex.batchInsert('workout_logs', [
            {
                planned_date: '2019-05-28',
                planned_weights: plannedData[0]['weight'],
                planned_reps: plannedData[0]['reps'],
                executed_date: now,
                executed_weights: dataObject.weight,
                executed_reps: dataObject.reps,
                ex_id: dataObject.ex_id,
                user_id: userID
            }
        ])
        return 'workout logged!'
    }
}
