export interface Data {
  score: number;
  keypoints: Keypoint[];
}

export interface Keypoint {
  position: Coordinate;
  part: string;
  score: number;
}

export interface Coordinate {
  y: number;
  x: number;
}
// Capitalize
export class postureIndicator {
  public nose: Coordinate;
  public leftEye: Coordinate;
  public rightEye: Coordinate;
  public leftEar: Coordinate;
  public rightEar: Coordinate;
  public leftShoulder: Coordinate;
  public rightShoulder: Coordinate;
  public leftElbow: Coordinate;
  public rightElbow: Coordinate;
  public leftWrist: Coordinate;
  public rightWrist: Coordinate;
  public leftHip: Coordinate;
  public rightHip: Coordinate;
  public leftKnee: Coordinate;
  public rightKnee: Coordinate;
  public leftAnkle: Coordinate;
  public rightAnkle: Coordinate;
  constructor(public pose: data) {
    this.nose = pose[0].position;
    this.leftEye = pose[1].position;
    this.rightEye = pose[2].position;
    this.leftEar = pose[3].position;
    this.rightEar = pose[4].position;
    this.leftShoulder = pose[5].position;
    this.rightShoulder = pose[6].position;
    this.leftElbow = pose[7].position;
    this.rightElbow = pose[8].position;
    this.leftWrist = pose[9].position;
    this.rightWrist = pose[10].position;
    this.leftHip = pose[11].position;
    this.rightHip = pose[12].position;
    this.leftKnee = pose[13].position;
    this.rightKnee = pose[14].position;
    this.leftAnkle = pose[15].position;
    this.rightAnkle = pose[16].position;
  }

  shoulderParallelFunct = (tolerance: number = 0.1): boolean => {
    if (
      (this.leftShoulder.y - this.rightShoulder.y) /
      (this.leftShoulder.x - this.rightShoulder.x) <
      tolerance &&
      (this.leftShoulder.y - this.rightShoulder.y) /
      (this.leftShoulder.x - this.rightShoulder.x) >
      -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  HipParallelFunct = (tolerance: number = 0.1): boolean => {
    //hip parallel
    if (
      (this.leftHip.y - this.rightHip.y) / (this.leftHip.x - this.rightHip.x) <
      tolerance &&
      (this.leftHip.y - this.rightHip.y) / (this.leftHip.x - this.rightHip.x) >
      -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  kneeParallelFunct = (tolerance: number = 0.1): boolean => {
    //knee parallel
    if (
      (this.leftKnee.y - this.rightKnee.y) /
      (this.leftKnee.x - this.rightKnee.x) <
      tolerance &&
      (this.leftKnee.y - this.rightKnee.y) /
      (this.leftKnee.x - this.rightKnee.x) >
      -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  ankleParallelFunct = (tolerance: number = 0.1): boolean => {
    //ankle parallel
    if (
      (this.leftAnkle.y - this.rightAnkle.y) /
      (this.leftAnkle.x - this.rightAnkle.x) <
      tolerance &&
      (this.leftAnkle.y - this.rightAnkle.y) /
      (this.leftAnkle.x - this.rightAnkle.x) >
      -tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  ankleDistEqualShoulderDist = (tolerance: number = 0.1): boolean => {
    const shoulderDistance = this.leftShoulder.x - this.rightShoulder.x;
    const ankleDistance = this.leftAnkle.x - this.rightAnkle.x;
    //ankle distance approx. equal to shoulder distance
    if (
      ankleDistance / shoulderDistance > 1 - tolerance &&
      ankleDistance / shoulderDistance < 1 + tolerance
    ) {
      return true;
    } else {
      return false;
    }
  };

  //determineKneePosition = ()

  turningFeetOutWhenDownLeft = (tolerance1: number = 0.1, tolerance2: number = 0.2) => {
    const result = this.findAnkleKneeSlope(this.leftAnkle.x, this.leftKnee.x, this.leftAnkle.y, this.leftKnee.y)
    //left ankle slope with left knee & right ankle slop with right knee
    if (result > tolerance1) {
      return false;
    } else if (result < -tolerance2) {
      return true;
    } else {
      return;
    }
  }

  turningFeetOutWhenDownRight = (tolerance1: number = 0.1, tolerance2: number = 0.2) => {

    //left ankle slope with left knee & right ankle slop with right knee
    const result = this.findAnkleKneeSlope(this.rightAnkle.x, this.rightKnee.x, this.rightAnkle.y, this.rightKnee.y)
    //left ankle slope with left knee & right ankle slop with right knee
    if (result < -tolerance1) {
      return false;
    } else if (result > tolerance2) {
      return true;
    } else {
      return;
    }
  }

  findAnkleKneeSlope = (x2: number, x1: number, y2: number, y1: number) => {
    return (x2 - x1) / (y2 - y1);
  }

  squatCount = (tolerance: number = 0.9) => {
    //hip get near/below knee level
    if (this.leftHip.y / this.leftKnee.y >= tolerance &&
      this.rightHip.y / this.rightKnee.y >= tolerance) {
        return true;
      } else {
        return false;
      }
  }

  squatUp = (tolerance: number = 0.4) => {
    let countDown = 0;
    let leftRatio = (this.leftHip.y - this.leftKnee.y) / (this.leftHip.y - this.leftAnkle.y)
    let rightRatio = (this.rightHip.y - this.rightKnee.y) / (this.rightHip.y - this.rightAnkle.y)
  
    if (this.squatCount()) {
      countDown = 1;
    }
    if (countDown == 1 && 
       leftRatio >= tolerance &&
      rightRatio >= tolerance) {
      return true;
    } else { 
      return false;
    }
    
  }

}
