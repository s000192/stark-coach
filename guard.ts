import * as express from 'express';
import * as path from 'path';
import { userService } from './main';
// import { userService } from './main';

export function isLoggedIn(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    if (req.user) {
        console.log('request dot user?');
        console.log(req.user);
        next();
    } else {
        res.redirect("/login");
    }
}

export async function hasPreference(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    const user = await userService.showUserDetail(req.user);
    // check if have goal_id
    if (user.goal_id) {
        next();
    } else {
        res.redirect("/users/preferences");
    }
}

export async function hasPlan(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    const user = await userService.showUserDetail(req.user);
    // check if have plan_id
    if (user.plan_id) {
        next();
    } else {
        res.redirect("/users/preview");
    }
}

export function isAdmin(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    if (req.user && req.user.level === "admin") {
        next();
    } else {
        res.redirect(path.join(__dirname + "/public/login/login.html"));
    }
}

export function isCoach(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    if (req.user && req.user.level === "coach") {
        next();
    } else {
        res.redirect(path.join(__dirname + "/public/login/login.html"));
    }
}
