import * as express from "express";
import * as bodyParser from "body-parser";
import * as expressSession from "express-session";
import * as morgan from "morgan";
// import * as jsonfile from "jsonfile";
import * as path from "path";
import * as Knex from "knex";
import * as passport from 'passport';
import { isLoggedIn, hasPlan, hasPreference /* isAdmin */ } from './guard';
// import { Request, Response } from "express";
import { UserRouter } from "./routers/UserRouter";
import { UserService } from "./services/UserService";
import { WorkoutService } from "./services/WorkoutService";
import { WorkoutRouter } from "./routers/WorkoutRouter";
import { SignupService } from "./services/SignupService";
import { SignupRouter } from "./routers/SignupRouter";

const app = express();

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const knexFile = require('./knexfile');
const knex = Knex(knexFile[process.env.NODE_ENV || 'development']);

// Session and Cookies
const sessionMiddleware = expressSession({
  secret: "Lifting weights with an AI coach",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false, maxAge: 604800000 }
});

app.use(sessionMiddleware);

//passport added as the middleware
app.use(passport.initialize());
app.use(passport.session());

import './passport';

// Routers
const signupService = new SignupService(knex);
const signupRouter = new SignupRouter(signupService);

export const userService = new UserService(knex);
const userRouter = new UserRouter(userService);

const workoutService = new WorkoutService(knex);
const workoutRouter = new WorkoutRouter(workoutService);

//const testing = async () => {
//  const MatchedPlan = await userService.showMatchedPlan(2);
//  console.log(MatchedPlan);
//  // await userService.checkGoal(2);
//  // return await userService.checkPlan(2);
//}
//testing();

app.get('/login',(req,res)=>{
  res.sendFile(path.join(__dirname,'/public/login/login.html'));
});

// loading page
app.get("/transition", async (req, res) => {
  res.sendFile(path.join(__dirname, "/public/transition/transition.html"));
});

app.post("/login",
  passport.authenticate('local', { failureRedirect: '/login.html' }),
  async (req, res) => {
    //if login successfully
    if(req.user && req.session) {
      req.session.isLoggedIn = true;
      req.session.currentUser = req.user.id;
    }
    //console.log(req.user);
    res.redirect('/');
  }
);

app.get('/auth/google/', passport.authenticate('google', {
  scope: ['email', 'profile'/*,'https://www.googleapis.com/auth/gmail.compose' */]
}));


app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: "/login" }),
  (req, res) => {
    res.redirect('/users.html')
  });

app.use("/signup", signupRouter.router());
app.use("/users", isLoggedIn, userRouter.router());
app.use("/workout", isLoggedIn, hasPreference, hasPlan, workoutRouter.router());

// Login & Sign up
app.get("/", async (req, res) => {
  res.redirect('/workout')
});

app.get("/signup", async (req, res) => {
  res.sendFile(path.join(__dirname, "/public/signup/signup.html"))
});

app.get('/logout', (req, res) => {
  req.logOut();
  res.redirect(path.join(__dirname, "/public/login/login.html"));
});

const PORT = 8080;

app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
