import * as passport from 'passport';
import { userService } from './main';
import * as passportLocal from 'passport-local';
import * as passportOAuth2 from 'passport-oauth2';
import { User } from './services/models';
import {checkPassword} from './hash';
import fetch from 'node-fetch';
import * as dotenv from 'dotenv';

dotenv.config();


const {GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET,SERVER_NAME} = process.env;


const LocalStrategy = passportLocal.Strategy;
const OauthStrategy = passportOAuth2.Strategy;

passport.use("local",new LocalStrategy(
    async function(username,password,done){
        const users = await userService.listUser();
        const user = users.find((user)=>user.username === username);
        if(!user){
            return done(null,false,{message:"Incorrect Username"});
        }
        const match = await checkPassword(password,user.hashed_password);
        if(match){
            return done(null,user);
        }else{
            return done(null,false,{message:"Incorrect Password"});
        }
    }
))

passport.use("google",new OauthStrategy({
    authorizationURL: 'https://accounts.google.com/o/oauth2/auth',
    tokenURL:"https://accounts.google.com/o/oauth2/token",
    clientID: GOOGLE_CLIENT_ID ? GOOGLE_CLIENT_ID :"",
    clientSecret: GOOGLE_CLIENT_SECRET? GOOGLE_CLIENT_SECRET: "",
    callbackURL: `${SERVER_NAME}/auth/google/callback`
}, async function(accessToken:string, refreshToken:string, profile:any, done:Function){
    const res = await fetch('https://www.googleapis.com/oauth2/v2/userinfo',{
        method:"get",
        headers:{
            "Authorization":`Bearer ${accessToken}`
        }
    });
    const result = await res.json();
    const users = await userService.listUser();
    const user = users.find((user)=>user.username == result.email);
    if(!user){
        return done(new Error("User not found"));
    }
    done(null,{accessToken,refreshToken,id:user.id})
}))

passport.serializeUser(function(user:User,done){
    done(null,user.id);
});


passport.deserializeUser(async function(id,done){
    const users = await userService.listUser();
    const user = users.find((user)=>user.id === id);

    if(user){
        // found user
        done(null,user.id);
    }else{
        // user not found
        done(new Error(`User with id ${id} not found!!`));
    }

});
