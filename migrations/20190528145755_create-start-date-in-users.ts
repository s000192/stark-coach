import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('users_workout');

    if(hasTable) {
        await knex.schema.table('users_workout', (table)=>{
            table.dropColumn("start_date");
        })
    };
    const hasTable2 = await knex.schema.hasTable('users');
    if(hasTable2) {
        await knex.schema.table('users', (table)=>{
            table.date("start_date");
        })
    };

};

exports.down = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('users_workout');

    if(hasTable) {
        await knex.schema.table('users_workout', (table)=>{
            table.date("start_date");
        })
    };
    const hasTable2 = await knex.schema.hasTable('users');
    if(hasTable2) {
        await knex.schema.table('users', (table)=>{
            table.dropColumn("start_date");
        })
    };
};