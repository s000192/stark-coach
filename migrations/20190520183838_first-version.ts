import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('users')&& knex.schema.hasTable('goals')&& knex.schema.hasTable('plan_templates')&& knex.schema.hasTable('detail_plans')&& knex.schema.hasTable('risks')&& knex.schema.hasTable('users_risk')&& knex.schema.hasTable('users_workout')&& knex.schema.hasTable('exercises') && knex.schema.hasTable('workout_log');

    if(!hasTable) {
        // can use createTable if not exists
        await knex.schema.createTable('goals', (table)=>{
            table.increments();
            table.string("goal").notNullable();
            table.timestamps(false, true);
        });

        await knex.schema.createTable('levels', (table)=>{
            table.increments();
            table.string("level").notNullable();
            table.timestamps(false, true);
        });

        await knex.schema.createTable('plan_templates', (table)=>{
            table.increments();
            table.string("name").notNullable();
            table.decimal("weight_increment").notNullable().defaultTo(2.5);
            table.integer("primary_goal_id");
            table.foreign("primary_goal_id").references("goals.id");
            table.integer("secondary_goal_id");
            table.foreign("secondary_goal_id").references("goals.id");
            table.integer("level_id");
            table.foreign("level_id").references("levels.id");
            table.timestamps(false, true);
        });

        await knex.schema.createTable('users', (table)=>{
            table.increments();
            table.string("username").notNullable();
            table.string("hashed_password").notNullable();
            table.string("gender", 1).notNullable().defaultTo("N");
            table.decimal("bodyweight", 5, 2).notNullable().defaultTo(0);
            table.integer("days_committed").notNullable().defaultTo(0);
            table.boolean("deload").notNullable().defaultTo(false);
            table.boolean("need_spotter").notNullable().defaultTo(true);
            table.integer("level_id").notNullable().defaultTo(3);
            table.foreign("level_id").references("levels.id");
            table.integer("goal_id");
            table.foreign("goal_id").references("goals.id");
            table.integer("plan_id");
            table.foreign("plan_id").references("plan_templates.id");
            table.timestamps(false, true);
        });

        await knex.schema.createTable('exercises', (table)=>{
            table.increments();
            table.string("name").notNullable();
            table.timestamps(false, true);
        });

        await knex.schema.createTable('detailed_plans', (table)=>{
            table.increments();
            table.integer("order_of_ex").notNullable();
            table.integer("sets").notNullable();
            table.integer("reps").notNullable();
            table.integer("rest_time").notNullable();
            table.integer("day").notNullable();
            table.integer("week");
            table.integer("ex_id");
            table.foreign("ex_id").references("exercises.id");
            table.integer("plan_id");
            table.foreign("plan_id").references("plan_templates.id");
            table.timestamps(false, true);
        });

        await knex.schema.createTable('risks', (table)=>{
            table.increments();
            table.string("name").notNullable();
            table.timestamps(false, true);
        });

        await knex.schema.createTable('users_risks', (table)=>{
            table.increments();
            table.integer("count").notNullable();
            table.integer("user_id");
            table.foreign("user_id").references("users.id");
            table.integer("risk_id");
            table.foreign("risk_id").references("risks.id");
            table.timestamps(false, true);
        });

        await knex.schema.createTable('users_workout', (table)=>{
            table.increments();
            table.integer("order_of_ex").notNullable();
            table.decimal("weight", 5, 2).notNullable();
            table.integer("sets").notNullable();
            table.integer("reps").notNullable();
            table.integer("rest_time").notNullable();
            table.integer("day").notNullable();
            table.integer("week");
            table.integer("ex_id");
            table.foreign("ex_id").references("exercises.id");
            table.integer("user_id");
            table.foreign("user_id").references("users.id");
            table.timestamps(false, true);
        });

        await knex.schema.createTable('workout_logs', (table)=>{
            table.increments();
            table.date("planned_date").notNullable();
            table.decimal("planned_weights", 5, 2).notNullable();
            table.integer("planned_reps").notNullable();
            table.date("executed_date");
            table.decimal("executed_weights", 5, 2);
            table.integer("executed_reps");
            table.integer("ex_id");
            table.foreign("ex_id").references("exercises.id");
            table.integer("user_id");
            table.foreign("user_id").references("users.id");
            table.timestamps(false, true);
        });
    }else{
        return await Promise.resolve();
    }
};

exports.down = async function (knex: Knex): Promise<any> {
    await knex.schema.dropTable('workout_logs');
    await knex.schema.dropTable('users_workout');
    await knex.schema.dropTable('users_risks');
    await knex.schema.dropTable('risks');
    await knex.schema.dropTable('detailed_plans');
    await knex.schema.dropTable('exercises');
    await knex.schema.dropTable('users');
    await knex.schema.dropTable('plan_templates');
    await knex.schema.dropTable('levels');
    await knex.schema.dropTable('goals');
};