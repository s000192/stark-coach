import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('plan_templates');

    if(hasTable) {
        await knex.schema.table('plan_templates', (table)=>{
            table.integer("level_id").notNullable().alter();
        })
    };
};

exports.down = async function (knex: Knex): Promise<any> {
    const hasTable = await knex.schema.hasTable('plan_templates');

    if(hasTable) {
        await knex.schema.table('plan_templates', (table)=>{
            table.integer("level_id").nullable().alter();
        })
    };
};
